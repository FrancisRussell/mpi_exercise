MACHINES=32
TRAPEZOIDS=5000
CXXFLAGS=-Wall -pedantic -g -O2 -std=c++11

all: pi

pi: pi.cpp
	mpiCC ${CXXFLAGS} pi.cpp -o $@

clean:
	rm -f pi.o pi

run: pi
	mpirun -np ${MACHINES} ./pi ${TRAPEZOIDS}

.PHONY: clean all run
