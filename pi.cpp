#include "mpi.h"
#include <cstdio>
#include <cstdlib>
#include <cassert>

static const int root = 0;

// Sends "count" double precision values from "values" to "dest". Is blocking.
static void send_double(const double *values, const int count, const int dest)
{
  MPI_Send(values, count, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
}

// Receives "count" double precision values to "values" to "source". Is blocking.
static void receive_double(double *values, const int count, const int source)
{
  MPI_Recv(values, count, MPI_DOUBLE, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

// Sums "send" from all nodes to "receive" on the root process.
static void sum_to_root(const double send, double &receive)
{
  MPI_Reduce(&send, &receive, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD);
}

static inline long double calculate_f(const long double x)
{
  return 4.0 / (1.0 + x * x);
}

int main(int argc, char* argv[])
{
  double time_begin;
  MPI_Init(&argc, &argv);
  MPI_Barrier(MPI_COMM_WORLD);
  time_begin = MPI_Wtime();

  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size); // Total number of processes
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); // ID of current process

  int trapezoids;
  if (rank==root)
  {
    if (argc <= 1)
      trapezoids = 1;
    else
      trapezoids = atoi(argv[1]);
  }

  MPI_Bcast(&trapezoids, 1, MPI_INT, root, MPI_COMM_WORLD);

  // Calculate trapezoid widths and indices
  const double width = 1.0 / trapezoids;
  const long start = (rank * trapezoids) / size;
  const long end = ((rank+1) * trapezoids) / size;

  // Detect wrap around errors
  assert(start <= end);

  double subresult = 0.0; // The contribution to the result from this process

  // Only execute if there is at least one trapezoid to evaluate.
  if (start < end)
  {
    // Add the edges of the outer trapezoids
    subresult += (calculate_f(start*width) + calculate_f(end*width)) / 2;

    // Add the edges of the inner trapezoids
    for(long index = start + 1; index < end; ++index)
    {
      const double x = index * width;
      subresult += calculate_f(x);
    }

    // Multiply the sum of heights by the width of trapezoid base
    subresult *= width;
  }

  // The final result
  double result = 0.0;

  // Sums contributions from every process to "result" on root process.
  sum_to_root(subresult, result);

  if (rank==root)
  {
    printf("Number of trapezoids: %d\t", trapezoids);
    printf("Number of processes: %d\t", size);
    printf("Time: %.5f seconds\t", MPI_Wtime() - time_begin);
    printf("Result: %.12f\n", result);
  }

  MPI_Finalize();
}
